#!/usr/bin/python
# -*- coding: utf-8 -*-
#TF041968 - Johan Persson - 2013

import os
import re
import math
import argparse

def main():
	"""Main method that parse arguments from command line"""	
	# Parser that require filename and language
	parser = argparse.ArgumentParser()
	parser.add_argument("filename", help="File to spell check", action="store")
	parser.add_argument("-l", "--lexicon", help="Lexicon file to spell check against", action="store")
	parser.add_argument("-o", "--output", help="Errors saved to specified filename. If no flag is set output.txt is created")
	args = parser.parse_args()	
	# Saving user input and start parsing process
	lexicon  = args.lexicon
	file_to_check = args.filename
	output_file = args.output
	Parse_File(lexicon, file_to_check, output_file)

def Parse_File(lexicon, filename, output):
	"""Validates input and starts parsing"""
	# If no output file is entered use standard
	if output is None:
		output = "output.txt"
	# If no lexicon is entered use standard created from NewsArticle.txt
	if not lexicon:
		lexicon = Create_standard_lexicon()
	# Check that the input is valid
	# If valid perform spell check on document.
	# Check if textfile to spell check exists
	if Check_file_exist(filename):
		# If given lexicon exists
		if Check_file_exist(lexicon):
			error_list = Check_spelling(lexicon, filename)
			print "Spell correction completed!"
			# If error list has errors
			if error_list:
				write_to_file(error_list, output)
				print "Errors can be found in " + output
			# Error list is empty
			else:
				print "No errors were found"
		# If lexicon is not found
		else:
			print "Lexicon not found"
	# If textfile to spell check not exists
	else:
		print "File not found"
		
def Check_file_exist(filename):
	"""Check so that the user given file exists"""
	# Return True or False
	return os.path.exists(filename)	

def Create_standard_lexicon():
	"""Create a standard lexicon parsed from a given textfile"""
	"""Removes any punctuation and/or other symbols"""
	# Open file and read it into a list
	token_file = open("NewsArticle.txt", "r")
	token_list = token_file.readlines()
	token_file.close()
	# Remove duplicates and punctuation from list 
	token_list = list(set(token_list))
	token_list = remove_punctuations(token_list)
	# Parse text from file above into lexicon.txt
	lexicon = "lexicon.txt"
	lexicon_file = open(lexicon, "w")
	lexicon_file.writelines(token_list)
	lexicon_file.close()
	return lexicon

def Check_spelling(lexicon, file_to_check):
	"""Check spelling in given file with chosen or standard lexicon"""
	# Create lists of file_to_check and lexicon
	article = open(file_to_check, "r")
	article_list = article.readlines()
	article_list = remove_punctuations(article_list)
	article.close()
	lexicon = open(lexicon, "r")
	lexicon_list = lexicon.readlines()
 	lexicon.close()
 	
 	# List for holding all errors and corrections
	error_list = []
	# Every word in the article
	for word in article_list:
		# If there's no match for the word in the lexicon
		if not word in lexicon_list:
			word = word.strip()
			# Holds all spelling suggestions that may apply
			suggestions = ""
			# Offset for when to accept a word
			# Floor of current word lengt / 2
			# If larger it's not taken for a misspelled word 
			accptbl_offset = math.floor(len(word)/2)
			# Every word in lexicon
			for index, lex_word in enumerate(lexicon_list):
				lex_word = lex_word.strip()
				# Get the words current offset
				offset = levenshtein_distance(word.strip(), lex_word.strip())
				# If offset is equal to the acceptable offset - Add word to suggestion
				if offset == accptbl_offset:
 					suggestions += "\t"+lex_word
 				# If offset is lower(better) than acceptable
 				# Clear suggestions and add word
 				# Lower accptbl offset to current offset
				if offset < accptbl_offset:
					suggestions = ""
					suggestions = "\t"+lex_word
					accptbl_offset = offset;
			# Concatenate wrong spelled word and suggestions
			final_string = word+suggestions
			# Add to list of errors
			error_list.append(final_string)	
	return error_list

def write_to_file(word_list, filename):
	"""Write list to file"""
	textfile = open(filename, "w")
	for line in word_list:
		textfile.write("%s\n" % line)

def remove_punctuations(list_to_clean):
	"""Removes punctuation from list"""
	# Strange, but i have to do it in 2 steps to get it to work.
	# I get a conflict between ) and . 
	for token in list_to_clean:
		token.decode("utf-8")
		if re.search(re.compile(ur"([(),!\'\";\?\-])"), token):
			list_to_clean.remove(token)
		token.strip()
	for token in list_to_clean:
		if re.match(re.compile(r"\."), token):
			list_to_clean.remove(token)
	return list_to_clean

def levenshtein_distance(first, second):
    """Find the Levenshtein distance between two strings. By Stavros Korokithakis"""
    """Got this straight from the web but i understand what it's doing."""
    if len(first) > len(second):
        first, second = second, first
    if len(second) == 0:
        return len(first)
    first_length = len(first) + 1
    second_length = len(second) + 1
    distance_matrix = [[0] * second_length for x in range(first_length)]
    for i in range(first_length):
       distance_matrix[i][0] = i
    for j in range(second_length):
       distance_matrix[0][j]=j
    for i in xrange(1, first_length):
        for j in range(1, second_length):
            deletion = distance_matrix[i-1][j] + 1
            insertion = distance_matrix[i][j-1] + 1
            substitution = distance_matrix[i-1][j-1]
            if first[i-1] != second[j-1]:
                substitution += 1
            distance_matrix[i][j] = min(insertion, deletion, substitution)
    return distance_matrix[first_length-1][second_length-1]
	
if __name__ == "__main__":
    main()